package laporan2;

public class masyarakatSekitar extends penduduk {

    private String nomor;

    public masyarakatSekitar() {
    }

    public masyarakatSekitar(String nomor, String tempatTgllahir, String datanama) {
        this.nomor = nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    double hitungIuran() {
        double iuran = Integer.parseInt(getNomor()) * 100;
        return iuran;
    }

}
