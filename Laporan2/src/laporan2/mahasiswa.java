package laporan2;

class mahasiswa extends penduduk {

    private String nim;

    public mahasiswa() {
    }

    public mahasiswa(String nim, String datanama, String tempatTgllahir) {
        this.nim = nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    double hitungIuran() {
        double iuran = Integer.parseInt(nim) / 1000;
        return iuran;
    }
}
