package laporan2;

public abstract class penduduk {

    private String nama;
    private String tempatTgllahir;

    public penduduk() {
    }

    public penduduk(String nama, String tempatTgllahir) {
        this.tempatTgllahir = tempatTgllahir;
        this.nama = nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setTempatTgllahir(String tempatTgllahir) {
        this.tempatTgllahir = tempatTgllahir;
    }

    public String getTempatTgllahir() {
        return tempatTgllahir;
    }

    abstract double hitungIuran();
}
